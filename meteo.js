const key = 'df7de645620cb045dd83341aeb3036e2'


let form = document.querySelector('form')

let api = function (city) {
    let url = `https://api.openweathermap.org/data/2.5/weather?q=${city},fr&units=metric&lang=fr&appid=${key}`
    fetch(url)
        .then(response => response.json().then((data) => {
            let myicon = data.weather[0].icon
            const icone =  `https://openweathermap.org/img/wn/${myicon}.png`
            console.log("mon data:", data)
            document.querySelector('#name').innerHTML = `${data.name}`
            document.querySelector("#temp").innerHTML = `<i class="bi bi-thermometer-low"></i><div id="tempmin">${data.main.temp_min}°C </div>` + `<i class="bi bi-thermometer-half"></i>${data.main.temp} °C` + `<i class="bi bi-thermometer-high"></i><div id="tempmax">${data.main.temp_max}°C </div>`
            if (data.main.feels_like !== data.main.temp){
                document.querySelector("#ress").innerHTML = "ressenti :  " + data.main.feels_like + "°C"
            }
            document.querySelector("#wind").innerHTML = `<i class="bi bi-wind"></i>${Math.round(data.wind.speed * 3.6)} km/h`
            document.querySelector("#humidity").innerHTML = `<i class="bi bi-droplet-half"></i>${data.main.humidity} %`
            document.querySelector("#weather_icon").innerHTML = `<img src ="https://openweathermap.org/img/wn/${myicon}@2x.png">`
            if (data.wind.deg > 330 && data.wind.deg <= 360) { 
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>EST`
            }

            if (data.wind.deg >= 290 && data.wind.deg < 330) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Sud-Est`
            }

            if (data.wind.deg > 250 && data.wind.deg < 290) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Sud`
            }
            if (data.wind.deg <= 250 && data.wind.deg > 200) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Sud-Ouest`
            }
            if (data.wind.deg < 200 && data.wind.deg > 160) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Ouest`
            }
            if (data.wind.deg <= 160 && data.wind.deg > 120) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Nord-Ouest`
            }
            if (data.wind.deg <= 120 && data.wind.deg > 80) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Nord`
            }
            if (data.wind.deg <= 80 && data.wind.deg > 40) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Nord-Est`
            }
            if (data.wind.deg >= 0 && data.wind.deg <= 40) {
                document.querySelector("#winddeg").innerHTML = `<i class="bi bi-compass"></i>Est`
            }


        }))
        .catch((err) => console.log("error :" + err))
}
api("Paris")

form.addEventListener('submit', function (e) {
    e.preventDefault()
    const ville = document.querySelector("#city_name").value
    api(ville)
})




// function showinfo(){
//     document.querySelector("div.name").innerHTML = data[0].name
//     document.querySelector("div.temp").innerHTML = data[0]
// }

// document.addEventListener("onClick", function (){
//     document.querySelector("div.name").innerHTML = data[0].name
//     document.querySelector("div.temp").innerHTML = data[0].temp
// })


// document.querySelector("form")
// document.addEventListener ("submit", function(e){
//     e.preventDefault()
//     const ville = document.querySelector("#city_name").value
// })